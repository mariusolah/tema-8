<?php
require('transport.class.php');

class Train extends Transport {
    public $railway;

    function __construct($railway) {
        $this->railway = $railway;
      }

      function set_name($railway) {
        $this->railway = $railway;
      }
      function get_name() {
        return $this->railway;
      }
}
?>