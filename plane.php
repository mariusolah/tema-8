<?php
require('transport.class.php');

class Plane extends Transport {
    public $airline;

    function __construct($airline) {
        $this->airline = $airline;
      }

      function set_name($airline) {
        $this->airline = $airline;
      }
      function get_name() {
        return $this->airline;
      }
}
?>